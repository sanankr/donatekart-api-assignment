# Donatekart API assignment

 **Assignment**
>
> Create a Web API with the following functionality. 
>
> 1. Add an endpoint to get a list of campaigns. 
Make a call to [external API](https://testapi.donatekart.com/api/campaign) to fetch the campaigns and sort them by Total Amount in descending order and return the campaigns. The result returned should contain the fields **Title, Total Amount, Backers Count and End Date**. 
> 2. Add an endpoint to get active campaigns that are created within the last 1 month. Make a call to [external API](https://testapi.donatekart.com/api/campaign) to fetch campaigns and filter active campaigns. A campaign is active if the end date is greater than or equal to today. Filter the list further to get the campaigns that are created within the last 30 days. 
> 3. Add an endpoint to get closed campaigns. 
Make a call to [external API](https://testapi.donatekart.com/api/campaign) to fetch campaigns and filter closed campaigns. A campaign is closed if the end date is less than today, or Procured Amount is greater than or equal to Total Amount.
 
**End points [Get]**
- api/campaign/list
- api/campaign/list/active/created-within-30-days
- api/campaign/list/closed

> All API calls returns a marshalled data with fields: **Title, Total Amount, Backers Count and End Date**, and sorted by **```Total Amount```** in descending order 
