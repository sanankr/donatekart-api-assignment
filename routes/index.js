const router = require("express").Router();

const campaignRouteHandler = require("./campaign");
/**
 * This module list & register all the entry level routes
 */

router.use("/campaign", campaignRouteHandler);

module.exports = router;
