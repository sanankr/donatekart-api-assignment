/**
 *
 * @param {[{code: string, title: string, featured: boolean, priority: number, shortDesc: string, imageSrc: string, created: string, endDate: string, totalAmount: number, procuredAmount: number, totalProcured: number, backersCount: number, categoryId: number, ngoCode: string, ngoName: string, daysLeft: number, percentage: number}]} data
 */
const marshallCampaignAndSortDsc = (data) => {
  return data
    .map(({ title, totalAmount, backersCount, endDate }) => ({
      title,
      totalAmount,
      backersCount,
      endDate,
    }))
    .sort(({ totalAmount: a }, { totalAmount: b }) => +b - +a);
};

module.exports = {
  marshallCampaignAndSortDsc,
};
