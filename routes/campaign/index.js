const router = require("express").Router();
const campaignBAL = require("../../BAL/campaign");

const apiHelper = require("./helper");

/**
 * API: /list
 *
 * Definition: List of campaigns
 *
 * Response Fields: Title, Total Amount, Backers Count and End Date
 * Required Fields: None
 * Optional Fields: None
 */
router.get("/list", async (req, res) => {
  const campaign = await campaignBAL.getAllCampaign();
  res.send(apiHelper.marshallCampaignAndSortDsc(campaign));
});

/**
 * API: /list/active/created-within-30-days
 *
 * Definition: List of campaigns active and created within 30 days
 *
 * Response Fields: Title, Total Amount, Backers Count and End Date
 * Required Fields: None
 * Optional Fields: None
 */
router.get("/list/active/created-within-30-days", async (req, res) => {
  const campaign = await campaignBAL.getActiveCampaignCreatedWithin30Days();
  res.send(apiHelper.marshallCampaignAndSortDsc(campaign));
});

/**
 * API: /list/closed
 *
 * Definition: List of campaigns which are closed
 *
 * Response Fields: Title, Total Amount, Backers Count and End Date
 * Required Fields: None
 * Optional Fields: None
 */
router.get("/list/closed", async (req, res) => {
  const campaign = await campaignBAL.getClosedCampaign();
  res.send(apiHelper.marshallCampaignAndSortDsc(campaign));
});

module.exports = router;
