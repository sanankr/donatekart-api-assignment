const { default: axios } = require("axios");
const conf = require("../config.json");

const fetchAllCampaign = async () => {
  const { campaignURL } = conf;
  const { data } = await axios.get(campaignURL);
  return data;
};

module.exports = { fetchAllCampaign };
