const express = require("express");
const cors = require("cors");

const apiRootHandler = require("./routes");

const app = express();
const port = 3001;

app.use(cors());
app.use("/api", apiRootHandler);

app.get("/", (req, res) => {
  res.send("Welcome to API Server");
});

app.listen(port, () => {
  console.log(`API Server is listening at port, ${port}.`);
});
