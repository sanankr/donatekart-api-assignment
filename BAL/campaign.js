const campaignDAL = require("../DAL/campaign");

const getAllCampaign = async () => {
  const campaigns = await campaignDAL.fetchAllCampaign();

  return campaigns;
};

const getActiveCampaignCreatedWithin30Days = async () => {
  const campaigns = await campaignDAL.fetchAllCampaign();

  const activeCampaign = filterActiveCampaign(campaigns);
  const activeCampaignCreatedWithin30Days =
    filterCampaignCreatedWithin30Days(activeCampaign);

  return activeCampaignCreatedWithin30Days;
};

const getClosedCampaign = async () => {
  const campaigns = await campaignDAL.fetchAllCampaign();

  const closedCampaign = filterClosedCampaign(campaigns);

  return closedCampaign;
};

/**
 *
 * @param {[{code: string, title: string, featured: boolean, priority: number, shortDesc: string, imageSrc: string, created: string, endDate: string, totalAmount: number, procuredAmount: number, totalProcured: number, backersCount: number, categoryId: number, ngoCode: string, ngoName: string, daysLeft: number, percentage: number}]} data
 *
 */
const filterActiveCampaign = (data) => {
  // Campaign is active if Today <= endDate

  const now = Date.now();
  return data.filter(({ endDate: endDateStr }) => {
    const endDate = new Date(endDateStr);
    return now <= endDate.getTime();
  });
};

/**
 *
 * @param {[{code: string, title: string, featured: boolean, priority: number, shortDesc: string, imageSrc: string, created: string, endDate: string, totalAmount: number, procuredAmount: number, totalProcured: number, backersCount: number, categoryId: number, ngoCode: string, ngoName: string, daysLeft: number, percentage: number}]} data
 */
const filterClosedCampaign = (data) => {
  // Campaign is closed if Today > endDate Or procuredAmount >= totalAmount

  const now = Date.now();
  return data.filter(({ endDate: endDateStr }) => {
    const endDate = new Date(endDateStr);
    return now > endDate.getTime();
  });
};

/**
 *
 * @param {[{code: string, title: string, featured: boolean, priority: number, shortDesc: string, imageSrc: string, created: string, endDate: string, totalAmount: number, procuredAmount: number, totalProcured: number, backersCount: number, categoryId: number, ngoCode: string, ngoName: string, daysLeft: number, percentage: number}]} data
 */
const filterCampaignCreatedWithin30Days = (data) => {
  const duration30DaysInMilliSec = 30 * 24 * 60 * 60 * 1000;
  const now = Date.now();

  return data.filter(({ created: createdDateStr }) => {
    const createdDate = new Date(createdDateStr);
    return now - createdDate.getTime() <= duration30DaysInMilliSec;
  });
};

module.exports = {
  getAllCampaign,
  getActiveCampaignCreatedWithin30Days,
  getClosedCampaign,
};
